clc;
close all;
clear;
format long g;
addpath ../'tstFcn';
%%


% costFunc = @Rastrigin;
% dim     = 50;
% var_min   =-5.12;
% var_max   = 5.12;

% costFunc = @StepFunc;
% dim     = 50;
% var_min   =-100;
% var_max   = 100;

costFunc = @Sphere;           
dim     = 50;
var_min   =-100;
var_max   = 100;

% costFunc = @rosen;              
% dim     = 50;
% var_min   =-30;
% var_max   = 30;


[ solutionHistory, res ] = MemoryBasedBigBangBigCrunch(costFunc, dim, var_min, var_max, 100, .3, 1000);
% plot(res);
min(res)

