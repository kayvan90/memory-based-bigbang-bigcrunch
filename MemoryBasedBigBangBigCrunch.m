function [ solutionHistory, fitnessHistory ] = MemoryBasedBigBangBigCrunch...
    (fitnessFunction, dim, var_min, var_max, nOfStars, HMCR, nOfIterations)
disp('memory based bb-bc optimization...');
% adjust varmax and varmin:
if size(var_max, 2) > 1
    num_of_clusters = evalin('base', 'num_of_clusters');
    var_max = repmat(var_max, 1, num_of_clusters);
    var_min = repmat(var_min, 1, num_of_clusters);
else
    % size is one so repmat the dimension:
    var_max = repmat(var_max, 1, dim);
    var_min = repmat(var_min, 1, dim);
end
% num_of_clusters = evalin('base', 'num_of_clusters');
% var_max = repmat(var_max, 1, num_of_clusters);
% var_min = repmat(var_min, 1, num_of_clusters);
%initialize:
numberOfTriesBeforVarChange = 10;
minTemperature = 1;
maxTemperature = 100;
Y = unifrnd(var_min, var_max, 1, dim);
YFitness = fitnessFunction(Y);
Var = 100;
nOfBBBC = nOfIterations;
counter = 0;
harmoneyMemorySize = 20;
dimension = dim;
solutionHistory = zeros(nOfIterations, dimension); 
fitnessHistory  = zeros(1, nOfIterations);

%***new:
HM = zeros(harmoneyMemorySize,dimension);
HMFitness = zeros(1,harmoneyMemorySize);
for i=1:harmoneyMemorySize
    HM(i,:) = unifrnd(var_min, var_max, 1, dim);
    HMFitness(i) = fitnessFunction(HM(i,:));
end
%***end new

for itr = 1:nOfBBBC
    HMCR = min( .88, HMCR + (nOfBBBC)/(nOfBBBC*nOfBBBC));
    % new formula for hmcr:
%     HMCR = .95 - HMCR - exp(-(3.5/nOfBBBC)*(itr/nOfBBBC)*itr)
%     disp(['itr :' num2str(itr) ' HMCR :' num2str(HMCR)]);
%     temperature = maxTemperature - itr*(maxTemperature - minTemperature)/nOfBBBC;
    star = BigBang(Y, nOfStars, var_min, var_max, Var, HM, HMCR, itr, nOfBBBC);
    [ newY, newYFitness, stars_fit ] = BigCrunch( star, fitnessFunction );
    %***new:
%     [worseHMFitness, worseHMIndex] = max(HMFitness);
%     if newYFitness < worseHMFitness 
%         HM(worseHMIndex,:) = newY;
%         HMFitness(worseHMIndex) = newYFitness;
%         %HMFitness'
%     end
    HM = [HM; newY; star];
    HMFitness = [HMFitness newYFitness stars_fit];
    [HMFitness, sort_idx] = sort(HMFitness);
    HM = HM(sort_idx, :);
    HM = HM(1: harmoneyMemorySize, :);
    HMFitness = HMFitness(1: harmoneyMemorySize);
    if rand < .7
        Y = HM(1, :);
    else
        Y = newY;
    end
%%%   ***end new
%     delta = newYFitness - YFitness; 
%     if delta < 0
%         counter = 0;
%         Y = newY;
%         YFitness = newYFitness;
%     elseif rand < exp(-delta/temperature)
%         counter = 0;
%         Y = newY;
%         YFitness = newYFitness;
%     else
%         counter = counter + 1;
%     end
%     if counter == numberOfTriesBeforVarChange
%         counter =0;
%         Var = max(Var-10*rand, rand);
% %         Var = Var * 2;
%     end
    solutionHistory(itr,:) = Y;
    fitnessHistory(itr) = YFitness;
%*****new:
[bestHMFitness, bestHMIndex] = min(HMFitness);
    solutionHistory(itr,:) = HM(bestHMIndex,:);
    fitnessHistory(itr) = HMFitness(bestHMIndex);

%     disp(['itr :' num2str(itr) ', HMCR :' num2str(HMCR) ', var :' num2str(Var) ', Fitness: ' num2str(bestHMFitness) ]);

end
end
%% Big Bang function:
function [ star ] = BigBang( X, numberOfStars, var_min, var_max, variance, HM, HMCR, itr, max_itr)

dimension = length(X);
STD = sqrt(variance);
star= zeros(numberOfStars, dimension);
smoothf = log(max_itr);
for i= 1:numberOfStars
    for j=1:dimension
        if rand < HMCR
            star(i,j) = HM(randi(size(HM,1)),j);
        else
%             star(i,j) = X(1,j) + (var_max(j)-var_min(j)) * STD * randn/(1+itr/smoothf);
            star(i,j) = normrnd(X(1,j), (var_max(j) - var_min(j))/(1+itr/smoothf));
%             star(i,j) = X(1,j) + 1 * (var_max(j) - var_min(j))/max_itr * randn;
        end
        %new:(ensure the value is between var_min and var_max)
         star(i,j) = min(max(star(i,j),var_min(j)),var_max(j));    
    end
end

end

%% Big Crunch function:
function [ Y , YFitness, stars_fit] = BigCrunch( star, fitnessFunc )
    [numberOfStars, dimension]= size(star);
    stars_fit = zeros(1,numberOfStars);
    for i = 1: numberOfStars
        stars_fit(1,i) = fitnessFunc(star(i,:));
    end
    f = 1./stars_fit;
    f = f/ sum(f);
    Y = zeros(1, dimension );
    for i=1:numberOfStars
        Y = Y + f(i)*star(i,:);
    end
    YFitness = fitnessFunc(Y);
end
